
import { EnvConfig } from "../src/env_config";
import { ResultWriter } from "../src/result_writer";
import { ServiceApi } from "../src/service_api";
const axios = require("axios").default;

Error.stackTraceLimit = 200;

interface AppObjects {
    config: EnvConfig;
    api: ServiceApi;
    stop: Function;
}

let cassandraClientTags: any;
let cassandraClientDynamicTags: any;
/**
 * Initialize the api object and its dependencies
 * 
 * @returns {object} A ready to use object ot test
 */
function initTest(): Promise<AppObjects> {
    Error.stackTraceLimit = 200;
    return new Promise((resolve, reject) => {
        // parse the configuration in the environnement variables
        const config = new EnvConfig();
        const resultWriter = new ResultWriter(config);
        const api = new ServiceApi(config, resultWriter);
        api.start();
        setTimeout(() => {
            resolve(<AppObjects>{
                "config": config,
                "api": api,
                "stop": function () {
                    api.stop();
                    resultWriter.stop();
                }
            });
        }, 1500);
    });
}

let init: AppObjects = <AppObjects>{};

beforeAll((done) => {
    initTest().then((objects) => {
        init = objects;
        done();
    });
}, 10000);

afterAll(() => {
    init.stop();
});


test("should do something", (done): void => {
    done();
}, 3000);
