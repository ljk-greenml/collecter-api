import { EnvConfig } from "../src/env_config";

test("should load the config fine", (): void =>{
    expect(new EnvConfig()).toBeDefined();
});