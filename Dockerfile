FROM node:19

WORKDIR /usr/src/app

COPY package.json ./
COPY package-lock.json ./

RUN npm install

COPY src ./src/
COPY Makefile .
COPY tsconfig.json .

RUN make build

CMD make run
