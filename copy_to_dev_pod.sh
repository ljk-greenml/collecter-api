#!/bin/bash

if [ "$KUBECFG" == "" ]
then
    echo "Please set KUBECFG envar"
    exit 0
fi

if [ "$1" == "" ]
then
    echo "You need to provide pod namespace";
    echo "Usage: ./copy_to_dev_pod.sh [namespace] [pod]";
    exit 0
fi

if [ "$2" == "" ]
then
    echo "You need to provide pod name";
    echo "Usage: ./copy_to_dev_pod.sh [namespace] [pod]";
    exit 0
fi


for srcfile in `ls src/*.ts`
do
    echo $srcfile
    kubectl --kubeconfig $KUBECFG cp -n ${1} ${srcfile} ${2}:/src/app/tags-service/${srcfile}
done

for srcfile in `ls tests/*.ts`
do
    echo $srcfile
    kubectl --kubeconfig $KUBECFG cp -n ${1} ${srcfile} ${2}:/src/app/tags-service/${srcfile}
done