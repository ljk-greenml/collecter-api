CREATE DATABASE greenml;
\c greenml
CREATE TABLE benchmark_measurement (
    timestamp_ms BIGINT NOT NULL,
    instance VARCHAR(255) NOT NULL,
    task  VARCHAR(255) NOT NULL,
    measurement  VARCHAR(255) NOT NULL,
    algo VARCHAR(255) NOT NULL,
    dataset  VARCHAR(255) NOT NULL,
    total_time_ms INT NOT NULL,
    consumption DOUBLE PRECISION NOT NULL,
    size_train INT,
    size_test INT,
    folds INT,
    -- binary classif
    m_accuracy DOUBLE PRECISION,
    m_precision DOUBLE PRECISION,
    m_recall DOUBLE PRECISION,
    m_f1 DOUBLE PRECISION,
    m_specificity DOUBLE PRECISION,
    m_auc DOUBLE PRECISION,
    m_log_loss DOUBLE PRECISION,
    m_iou DOUBLE PRECISION,
    -- multinomial classif
    m_precision_macro DOUBLE PRECISION,
    m_preicison_micro DOUBLE PRECISION,
    m_recall_macro DOUBLE PRECISION,
    m_recall_micro DOUBLE PRECISION,
    m_f1_macro DOUBLE PRECISION,
    m_f1_micro DOUBLE PRECISION,
    -- clustering
    m_adjusted_randindex DOUBLE PRECISION,
    m_rand_index DOUBLE PRECISION,
    m_h_score DOUBLE PRECISION,
    m_fowlkes_mallows_index DOUBLE PRECISION,
    -- regression
    m_r2 DOUBLE PRECISION,
    m_explained_variance DOUBLE PRECISION,
    m_mse DOUBLE PRECISION,

    PRIMARY KEY (timestamp_ms, task, dataset, algo)
);