/**
 * Module definition the ResultWriter class and it structs.
 * @module ResultWriter
 */

import { InfluxDB, FieldType } from "influx";
import { EnvConfig } from "./env_config";
import { Pool, QueryResult } from "node-postgres";

/**
 * Class that will instanciate db drivers and send the api received results
 * to influxdb.
 */
export class ResultWriter {
    // private members
    private config: EnvConfig;
    private influx: InfluxDB;
    private postgresPool: Pool;

    // constructor
    public constructor(config: EnvConfig) {
        this.config = config
        
        // initialize the database
        this.influx = new InfluxDB({
            host: config.INFLUXDB_HOST,
            port: config.INFLUXDB_PORT,
            database: 'reporting_greenml',
            schema: [
              {
                measurement: 'health_checks',
                fields: {
                  timestamp: FieldType.INTEGER,
                  duration: FieldType.INTEGER
                },
                tags: [
                  'instance'
                ]
              }, {
                measurement: 'benchmark_results_composite',
                fields: {
                  // general settings
                  timestamp: FieldType.INTEGER,
                  totalTimeMs: FieldType.INTEGER,
                  consumption: FieldType.FLOAT,
                  sizeTrain: FieldType.INTEGER,
                  sizeTest: FieldType.INTEGER,
                  folds: FieldType.INTEGER,
                  // binary classif
                  accuracy: FieldType.FLOAT,
                  precision: FieldType.FLOAT,
                  recall: FieldType.FLOAT,
                  f1: FieldType.FLOAT,
                  specificity: FieldType.FLOAT,
                  auc: FieldType.FLOAT,
                  logLoss: FieldType.FLOAT,
                  IoU: FieldType.FLOAT,
                  // multimodal classif
                  precisionMacro: FieldType.FLOAT,
                  precisionMicro: FieldType.FLOAT,
                  recallMacro: FieldType.FLOAT,
                  recallMicro: FieldType.FLOAT,
                  f1Macro: FieldType.FLOAT,
                  f1Micro: FieldType.FLOAT,
                  // clustering
                  adjustedRandIndex: FieldType.FLOAT,
                  randIndex: FieldType.FLOAT,
                  hScore: FieldType.FLOAT,
                  fowlkesMallowsIndex: FieldType.FLOAT,
                  // regression
                  r2: FieldType.FLOAT,
                  explainedVarianceScore: FieldType.FLOAT,
                  MSE: FieldType.FLOAT,

                },
                tags: [
                  'instance', 'task', 'measurement', "algorithm", "dataset"
                ]
              }
            ]
           })

           this.postgresPool = new Pool({
            user: config.POSTGRES_USER,
            database: 'postgres',
            password: config.POSTGRES_PASS,
            port: 5432,
            host: config.POSTGRES_HOST,
          })
    }

    /**
     * stop cleans the drivers and prepare to delete
     */
    public stop() {
        // TODO
    }

    /**
     * writeHealthCheck will write the result of a health
     * check into redis.
     * @param {HealthCheckInfos} hc A structure with informations about
     * the health that we write to influxdb timeseries.
     */
    public writeHealthCheck(hc: HealthCheckInfos): Promise<void> {
        return this.influx.writePoints([
            {
                measurement: 'health_checks',
                tags: { instance: hc.Instance },
                fields: {
                    timestamp: hc.Timestamp,
                    duration: hc.ResponseTimeMs
                },
            }
        ])
    }

    /**
     * writeBenchmarkResult will write the result of a benchmark
     * check into redis.
     * @param {BenchmarkResult} br Result of a benchmark to write.
     */
    public writeBenchmarkResult(br: BenchmarkResult): Promise<void> {
        return this.influx.writePoints([
            {
                measurement: 'benchmark_results_composite',
                tags: { instance: br.instance, task: br.task, measurement: br.measurement, dataset: br.dataset, algorithm: br.algorithm },
                fields: {
                    // general settings (in rows)
                    timestamp: br.timestamp,
                    totalTimeMs: br.totalTimeMs,
                    consumption: br.consumption,
                    sizeTrain: br.sizeTrain,
                    sizeTest: br.sizeTest,
                    folds: br.folds,
                    // binary classif
                    accuracy: br.accuracy,
                    precision: br.precision,
                    recall: br.recall,
                    f1: br.f1,
                    specificity: br.specificity,
                    auc: br.auc,
                    logLoss: br.logLoss,
                    IoU: br.IoU,
                    // multimodal classif
                    precisionMacro: br.precisionMacro,
                    precisionMicro: br.precisionMicro,
                    recallMacro: br.recallMacro,
                    recallMicro: br.recallMicro,
                    f1Macro: br.f1Macro,
                    f1Micro: br.f1Micro,
                    // clustering
                    adjustedRandIndex: br.adjustedRandIndex,
                    randIndex: br.randIndex,
                    hScore: br.hScore,
                    fowlkesMallowsIndex: br.fowlkesMallowsIndex,
                    // regression
                    r2: br.r2,
                    explainedVarianceScore: br.explainedVarianceScore,
                    MSE: br.MSE,
                },
            }
        ]).then(()=>{
          return this.postgresPool.query(
            "INSERT INTO benchmark_measurement (timestamp_ms, instance, task, "+
            "measurement, algo, dataset, total_time_ms, consumption, size_train,"+
            "size_test, folds, m_accuracy, m_precision, m_recall, m_f1, m_specificity,"+
            "m_auc, m_log_loss, m_iou, m_precision_macro, m_preicison_micro, "+
            "m_recall_macro, m_recall_micro, m_f1_macro, m_f1_micro, m_adjusted_randindex,"+
            "m_rand_index, m_h_score, m_fowlkes_mallows_index, m_r2, m_explained_variance,"+
            "m_mse) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,"+
            " $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28, $29,"+
            " $30, $31, $32)",
            [
              br.timestamp,
              br.instance,
              br.task,
              br.measurement,
              br.algorithm,
              br.dataset,
              br.totalTimeMs,
              br.consumption,
              br.sizeTrain,
              br.sizeTest,
              br.folds,
              br.accuracy,
              br.precision,
              br.recall,
              br.f1,
              br.specificity,
              br.auc,
              br.logLoss,
              br.IoU,
              br.precisionMacro,
              br.precisionMicro,
              br.recallMacro,
              br.recallMicro,
              br.f1Macro,
              br.f1Micro,
              br.adjustedRandIndex,
              br.randIndex,
              br.hScore,
              br.fowlkesMallowsIndex,
              br.r2,
              br.explainedVarianceScore,
              br.MSE
            ]
          );
        }).then((): void =>{ return });
    }
}

export interface HealthCheckInfos {
    Instance: string,
    Timestamp: number,
    ResponseTimeMs: number
}


export interface BenchmarkResult {
    // general settings (partitioning keys)
    instance: string,
    algorithm: string,
    dataset: string,
    task: string,
    measurement: string,
    // general settings (in rows)
    timestamp: number,
    totalTimeMs: number,
    consumption: number,
    sizeTrain: number,
    sizeTest: number,
    folds: number,
    // binary classif
    accuracy: number,
    precision: number,
    recall: number,
    f1: number,
    specificity: number,
    auc: number,
    logLoss: number,
    IoU: number,
    // multimodal classif
    precisionMacro: number,
    precisionMicro: number,
    recallMacro: number,
    recallMicro: number,
    f1Macro: number,
    f1Micro: number,
    // clustering
    adjustedRandIndex: number,
    randIndex: number,
    hScore: number,
    fowlkesMallowsIndex: number,
    // regression
    r2: number,
    explainedVarianceScore: number,
    MSE: number,
}