// env_config.ts
/**
 * This module defines the *EnvConfig* class that
 * parses the right environnement variables and store
 * them to be passed around the different other classes in the constructor
 *
 * @module EnvConfig
 */


/**
 * Class that parses environement variables
 */
export class EnvConfig {
    // public members
    INFLUXDB_HOST: string;
    INFLUXDB_PORT: number;
    SECRET_KEY: string;
    BASE_URL: string;
    PORT: number;
    POSTGRES_HOST: string;
    POSTGRES_USER: string;
    POSTGRES_PASS: string;

    constructor() {
        // get host and keyspaces of the cassandra/scylla server
        let INFLUXDB_HOST_ENV: unknown = process.env.INFLUXDB_HOST;
        let INFLUXDB_PORT_ENV: unknown = process.env.INFLUXDB_PORT;
        let SECRET_KEY_ENV: unknown = process.env.SECRET_KEY;
        let POSTGRES_HOST_ENV: unknown = process.env.POSTGRES_HOST;
        let POSTGRES_USER_ENV: unknown = process.env.POSTGRES_USER;
        let POSTGRES_PASS_ENV: unknown = process.env.POSTGRES_PASS;
        let INFLUXDB_HOST: string;
        let INFLUXDB_PORT: number;
        // test if envar exists and parse em
        if (typeof INFLUXDB_HOST_ENV == "string" && typeof INFLUXDB_PORT_ENV == "string") {
            INFLUXDB_HOST = INFLUXDB_HOST_ENV;
            INFLUXDB_PORT = parseInt(INFLUXDB_PORT_ENV);
            if (isNaN(INFLUXDB_PORT) || INFLUXDB_PORT<1) {
                console.error("INFLUXDB_PORT is not an integer");
                process.exit(0);
            }
        } else {
            console.debug("Missing INFLUXDB_HOST or INFLUXDB_PORT");
            process.exit(0);
        }

        let BASE_URL_ENV: unknown = process.env.BASE_URL;
        if(typeof BASE_URL_ENV == "string") {
            this.BASE_URL = BASE_URL_ENV;
        } else {
            this.BASE_URL = "";
        }

        let PORT_TXT: unknown = process.env.PORT;
        if(typeof PORT_TXT == "string" && PORT_TXT!="") {
            this.PORT = parseInt(PORT_TXT);
        } else {
            this.PORT = 80;
        }

        this.INFLUXDB_HOST = INFLUXDB_HOST;
        this.INFLUXDB_PORT = INFLUXDB_PORT;

        if (typeof SECRET_KEY_ENV == "string") {
            this.SECRET_KEY = SECRET_KEY_ENV;
        } else {
            console.log("You didn't set any SECRET_KEY envar, defaulting to unsecure phrase !!")
            this.SECRET_KEY = "this deployment is not secure";
        }

        // handle POSTGRESQL variables
        if (typeof POSTGRES_HOST_ENV == "string") {
            this.POSTGRES_HOST = POSTGRES_HOST_ENV;
        } else {
            console.debug("Missing POSTGRES_HOST");
            process.exit(0);
        }

        if (typeof POSTGRES_USER_ENV == "string") {
            this.POSTGRES_USER = POSTGRES_USER_ENV;
        } else {
            console.debug("Missing POSTGRES_USER");
            process.exit(0);
        }

        if (typeof POSTGRES_PASS_ENV == "string") {
            this.POSTGRES_PASS = POSTGRES_PASS_ENV;
        } else {
            console.debug("Missing POSTGRES_PASS");
            process.exit(0);
        }
    }

    /**
     * Will dump the envar to stderr (debug channel)
     */
    public displayValues(): void {
        console.debug("INFLUXDB_HOST=",this.INFLUXDB_HOST);
        console.debug("INFLUXDB_PORT=",this.INFLUXDB_PORT);
        console.debug("POSTGRES_HOST=", this.POSTGRES_HOST);
    }
}