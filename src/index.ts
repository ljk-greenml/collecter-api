/**
 * Cette api réceptionne les métriques (de fonctionnement et de performance/conso)
 * des benchmark du logicel pyGreenML.
 */


// importing depedencies
import { EnvConfig } from "./env_config";
import { ServiceApi } from "./service_api";
import { ResultWriter } from "./result_writer";

// parse the configuration in the environnement variables
const config = new EnvConfig();
config.displayValues();

// Create a class to write the results
const resultWriter = new ResultWriter(config);

// create the api object
const api = new ServiceApi(config, resultWriter);

// start the service
console.debug("Starting the API...");
api.start();

