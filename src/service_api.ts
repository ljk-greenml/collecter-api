// service_api.ts
/**
 * This module creates a class that implement the REST api
 * with express that is used to edit the dynamic tags.
 * 
 * @module ServiceApi
 */


import { EnvConfig } from "./env_config";
import { BenchmarkResult, HealthCheckInfos, ResultWriter } from "./result_writer";
const cors = require("cors");
var express = require("express");

/**
 * Class that create an HTTP api with the endpoint we need
 * for tags editing.
 */
export class ServiceApi {

    private config: EnvConfig;
    private started: boolean;
    private expressApp: any;
    private httpServer: any;
    private resultWriter: ResultWriter;

    /**
     * 
     * @param {EnvConfig} config The configuration object
     * @param {ResultWriter} resultWriter The object that handles writing to the db
     */
    public constructor(config: EnvConfig, resultWriter: ResultWriter) {

        // create express app
        this.expressApp = express();
        this.expressApp.use(cors());
        this.started = false;

        // parse body for json objects
        this.expressApp.use(express.json());
        this.expressApp.use(express.urlencoded({ extended: true }));

        // define url prefix strippin middleware to remove gateway additional url
        this.expressApp.use((req: any, _: any, next: any) => {
            if (config.BASE_URL != "" && config.BASE_URL != undefined) {
                req.url = req.url.replace(config.BASE_URL, "/");
            }
            next();
        });

        // save the callbacks for each endpoint
        this.config = config;
        this.resultWriter = resultWriter;

        // defines callback to receive uploaded structs
        this.expressApp.post("/report/health", (req: any, res: any) => {
            // validate request
            this.validateHealthReportRequest(req, res).then((ok: boolean) => {
                if (ok == true) {
                    let hc = this.parseHealthCheckResult(req);
                    this.resultWriter.writeHealthCheck(hc).then(()=>{
                        res.status(200).json({"message": "Ok"});
                        return;
                    }).catch((err: Error)=>{
                        console.error(err);
                        res.status(500).json({"message": "Internal Error"});
                    });
                }
            }).catch((err: Error)=>{
                console.error(err);
                res.status(500).json({"message": "Internal Error"});
            });
        });
        // 
        this.expressApp.post("/report/benchmark", (req: any, res: any) => {
            // validate request
            this.validateBenchmarkReportRequest(req, res).then((ok: boolean) => {
                if (ok == true) {
                    let br = this.parseBenchmarkResult(req);
                    this.resultWriter.writeBenchmarkResult(br).then(()=>{
                        res.status(200).json({"message": "Ok"});
                        return;
                    }).catch((err: Error)=>{
                        console.error(err);
                        res.status(500).json({"message": "Internal Error"});
                    });
                } else {
                    console.log("failed to validate benchmark result model")
                }
            }).catch((err: Error)=>{
                console.error(err);
                res.status(500).json({"message": "Internal Error"});
            });
        });

    }

    /**
     * Start the REST api
     */
    public start(): Promise<void> {
        return new Promise((resolve, reject): void => {
            if (this.started == true) {
                resolve();
            } else {
                this.started = true;
                this.httpServer = this.expressApp.listen(this.config.PORT, () => {
                    console.debug("App started on port " + this.config.PORT);
                    resolve();
                });
            }
        });
    }

    /**
     * stop will stop the http server
     *
     * @returns {Promise<void>} A promise that resolves when closing is done
     */
    public stop(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.started == true) {
                this.started = false;
                this.httpServer.close();
                resolve();
            } else {
                resolve();
            }
        });
    }

    /**
     * validateHealthReportRequest make sure the request has 
     * valid parameters to fill HealthCheck model.
     *
     * @param {any} req the Express request handler
     * @param {any} res the Express response handler
     * @returns {Promise<boolean>} true if the request is valid, false
     * if the ids are missing or not found. If false is returned, you need
     * to make sure NOT TO return anything to the API user with the res Express object as
     * it's already taken care of here.
     */
    private validateHealthReportRequest (req: any, res: any): Promise<boolean> {
        return new Promise((resolve, reject) => {

            if(typeof req.body.secret_key != "string" || req.body.secret_key != this.config.SECRET_KEY) {
                console.log("Request arrived with invalid security key of type "+(typeof req.body.secret_key))
                this.badRequestHandler(req, res);
                resolve(false);
                return;
            }

            // function top check for times when to stop
            let stop: boolean = false;
            let stopIfInvalid: Function = () => {
                if (stop == true) {
                    resolve(false);
                    return;
                }
            };

            for(let paramname in ["instance", "timestamp", "response_time_ms"]) {
                stop = this.handleUndefinedBodyParam(paramname, req, res);
                stopIfInvalid();
            }

            resolve(true);
        });
    }


    /**
     * validateBenchmarkReportRequest make sure the request has 
     * valid parameters to fill BnechmarkResult model.
     *
     * @param {any} req the Express request handler
     * @param {any} res the Express response handler
     * @returns {Promise<boolean>} true if the request is valid, false
     * if the ids are missing or not found. If false is returned, you need
     * to make sure NOT TO return anything to the API user with the res Express object as
     * it's already taken care of here.
     */
    private validateBenchmarkReportRequest (req: any, res: any): Promise<boolean> {
        return new Promise((resolve, reject) => {

            if(typeof req.body.secret_key != "string" || req.body.secret_key != this.config.SECRET_KEY) {
                this.notAuthorizedRequestHandler(req, res);
                resolve(false);
                return;
            }

            let params = [
                // general settings (partitioning keys)
                "instance",
                "algorithm",
                "dataset",
                "task",
                "measurement",
                // general settings (in rows)
                "timestamp",
                "total_time_ms",
                "consumption",
                "size_train",
                "size_test",
                "folds",
                // binary classif
                "accuracy",
                "precision",
                "recall",
                "f1",
                "specificity",
                "auc",
                "log_loss",
                "IoU",
                // multimodal classif
                "precision_macro",
                "precision_micro",
                "recall_macro",
                "recall_micro",
                "f1_macro",
                "f1_micro",
                // clustering
                "adjusted_randindex",
                "rand_index",
                "h_score",
                "fowlkes_mallows_index",
                // regression
                "r2",
                "explained_variance_score",
                "MSE",
            ];

            // function top check for times when to stop
            let stop: boolean = false;
            let stopIfInvalid: Function = () => {
                if (stop == true) {
                    resolve(false);
                    return;
                }
            };

            for(let i in params) {
                stop = this.handleUndefinedBodyParam(params[i], req, res);
                if(stop) {
                    console.log("missing parameter: ", params[i])
                }
                stopIfInvalid();
            }

            resolve(true);
        });
    }

    private parseHealthCheckResult (req: any): HealthCheckInfos {
        return <HealthCheckInfos>{
            "Instance": req.body.instance,
            "Timestamp": req.body.timestamp,
            "ResponseTimeMs": req.body.response_time_ms
        }
    }

    private parseBenchmarkResult (req: any): BenchmarkResult {
        return <BenchmarkResult>{
            // general settings (partitioning keys)
            instance: req.body.instance,
            algorithm: req.body.algorithm,
            dataset: req.body.dataset,
            task: req.body.task,
            measurement: req.body.measurement,
            // general settings (in rows)
            timestamp: req.body.timestamp,
            totalTimeMs: req.body.total_time_ms,
            consumption: req.body.consumption,
            sizeTrain: req.body.size_train,
            sizeTest: req.body.size_test,
            folds: req.body.folds,
            // binary classif
            accuracy: req.body.accuracy,
            precision: req.body.precision,
            recall: req.body.recall,
            f1: req.body.f1,
            specificity: req.body.specificity,
            auc: req.body.auc,
            logLoss: req.body.log_loss,
            IoU: req.body.IoU,
            // multimodal classif
            precisionMacro: req.body.precision_macro,
            precisionMicro: req.body.precision_micro,
            recallMacro: req.body.recall_macro,
            recallMicro: req.body.recall_micro,
            f1Macro: req.body.f1_macro,
            f1Micro: req.body.f1_micro,
            // clustering
            adjustedRandIndex: req.body.adjusted_randindex,
            randIndex: req.body.rand_index,
            hScore: req.body.h_score,
            fowlkesMallowsIndex: req.body.fowlkes_mallows_index,
            // regression
            r2: req.body.r2,
            explainedVarianceScore: req.body.explained_variance_score,
            MSE: req.body.MSE,
        }
    }

    /**
     * Will test if a body parameter is provided in an HTTP request using
     * the Express library request handler
     * 
     * @param {string} param The name of the body parameter to check existence of
     * @param {any} request The Express request handler object for HTTP reqs
     * @param {any} response The Express response handler object for HTTP reqs
     * @returns {boolean} If true, the caller needs to stop as an error was returned
     * with the response handler provided. If false, the body parameter exists and can be
     * further tested for type.
     */
    private handleUndefinedBodyParam(param: string, request: any, response: any): boolean {
        // if the parameter does not exists, return a 400 bad request
        if (Object.prototype.hasOwnProperty.call(request, "body") == false ||
            Object.prototype.hasOwnProperty.call(request.body, param) == false) {
            this.badRequestHandler(request, response);
            return true;
        } else {
            return false;
        }
    }

    /**
     * badRequestHandler returns an http response
     * for a Bad Request
     * 
     * @param {any} req the Express http request handler
     * @param {any} res the Express http response handler
     */
    private badRequestHandler(req: any, res: any): void {
        res.status(400).json({
            "message": "Bad Request"
        });
    }

    /**
     * notAuthorizedRequestHandler returns an http response
     * for a not authorized http error
     * 
     * @param {any} req the Express http request handler
     * @param {any} res the Express http response handler
     */
     private notAuthorizedRequestHandler(req: any, res: any): void {
        res.status(403).json({
            "message": "Forbidden"
        });
    }

    

    /**
     * notFoundHandler returns an http response
     * for a Not Found error
     * 
     * @param {any} req the Express http request handler
     * @param {any} res the Express http response handler
     */
    private notFoundHandler(req: any, res: any): void {
        res.status(404).json({
            "message": "Not Found"
        });
    }


    /**
     * internalErrorHandler returns an http response
     * for an Internal Server Error
     * 
     * @param {any} req the Express http request handler
     * @param {any} res the Express http response handler
     */
    private internalErrorHandler(req: any, res: any): void {
        res.status(500).json({
            "message": "Internal Server Error"
        });
    }

}