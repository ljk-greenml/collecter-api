CC=./node_modules/.bin/tsc

build:
	$(CC)

install:
	npm install

test:
	./node_modules/jest/bin/jest.js tests/*.test.ts --setupFiles dotenv/config --verbose --coverage --runInBand

run:
	node ./dist/index.js

env:
	set -a; source .env; set +a